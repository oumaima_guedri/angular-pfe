<!doctype html>
<!--[if lte IE 9 ]><html class="ie9"><![endif]-->
<!--[if gt IE 9]><!-->
<html lang="fr">
<!--<![endif]-->
<head>
<title>Une jeune femme vir&eacute;e de sa salle de sport en raison de sa tenue - Cosmopolitan.fr</title>
<script>
var pageEnvironment = {"route":"article","context":{"currentSectionId":2511248,"currentSectionLevel":4,"isConnected":false,"isSubscriber":false,"entryId":2006,"thematicId":2511247,"sectionId":2511248,"articleId":2026050,"articleTypeId":1,"levels":[{"id":2000,"title":"Cosmopolitan","reference":"accueil-cosmo","levelType":0},{"id":2006,"title":"Psycho","reference":"moi-moi-moi","levelType":1},{"id":2511247,"title":"Soci\u00e9t\u00e9","reference":"societe","levelType":2},{"id":2511248,"title":"Actus soci\u00e9t\u00e9","reference":",societe","levelType":4},{"id":2026050,"title":"Une jeune femme vir\u00e9e de sa salle de sport car sa tenue\u00a0\"perturbait\" les hommes","reference":"une-jeune-femme-viree-de-sa-salle-de-sport-car-sa-tenue-perturbait-les-hommes","levelType":5}]},"article":{"mainTrafficSourceId":2,"originId":1,"datePublication":"2019-01-30T11:07:18+01:00","dateModification":"2019-01-30T11:22:07+01:00","authorId":514503}};
!function(){var e=[],t=Object.defineProperty;t(e,"TYPES",{value:{CSS:"css",SCRIPT:"script"}});var n={},r=function(e,t){return t=Object.assign({href:e,rel:"stylesheet",type:"text/css",media:"all"},t||{}),a("link",t)},i=function(e,t){return t=Object.assign({src:e},t||{}),a("script",t)},a=function(e,t){var n=document,r=n.createElement(e);if("object"==typeof t)for(var i in t)r[i]=t[i];var a=n.getElementsByTagName("script")[0];return a.parentNode.insertBefore(r,a),r};Promise="function"==typeof Promise?Promise:function(){this.then=function(e){return e(),new Promise},this["catch"]=function(e){return e(),new Promise}};var s=function(t,a,s,o){if(!n[t]||o){var c;if(s=(s+"").toLowerCase(),s==e.TYPES.CSS?c=r:s==e.TYPES.SCRIPT?c=i:(match=t.match(/^[^?#]+\.([^.?#]+)(?:[?#].+)?$/))&&("css"==match[1]?c=r:"js"==match[1]&&(c=i)),!c)throw"Unknow resource type: "+t;var u=c(t,a);n[t]=new Promise(function(e,t){u.addEventListener("load",function(){e()}),u.addEventListener("error",function(){t()})})}return n[t]},o=function(){for(var e,t=[],n=0;n<arguments.length;++n)n?t.push(arguments[n]):e=arguments[n];var r=typeof e;"string"!=r&&("object"!=r||e instanceof Array)||(e=[e]);var i=[];e instanceof Array&&e.forEach(function(e){"string"==typeof e&&(e={url:e}),"object"==typeof e&&"string"==typeof e.url&&i.push(s(e.url,e.attributes,e.type,e.forceReload))});var a=Promise.all(i);return t.length&&(a=a.then(function(){return o.apply(null,t)})),a};t(e,"loadResources",{value:o}),window.mc2m=e}()
</script>
<link href="https://cache.cosmopolitan.fr/data/display/page.css?f5lrzgcc0740040gogsc8gsw4" media="all" rel="stylesheet" type="text/css"/>
<link href="https://cache.cosmopolitan.fr/data/display/article.css?bkpeke6ff7s4sw04kcg0os00c" media="all" rel="stylesheet" type="text/css"/>
<link rel="manifest" href="https://www.cosmopolitan.fr/manifest.json" />
<meta property="fb:app_id" content="227173460735079" />
<meta property="og:site_name" content="Cosmopolitan.fr" />
<meta property="twitter:card" content="summary_large_image" />
<meta property="og:title" content="Une jeune femme virée de sa salle de sport car sa tenue &quot;perturbait&quot; les hommes" />
<meta property="twitter:title" content="Une jeune femme virée de sa salle de sport car sa tenue &quot;perturbait&quot; les hommes" />
<meta property="og:url" content="https://www.cosmopolitan.fr/une-jeune-femme-viree-de-sa-salle-de-sport-car-sa-tenue-perturbait-les-hommes,2026050.asp" />
<meta property="og:image" content="https://cache.cosmopolitan.fr/data/photo/w1200_h630_ci/5b/femme-salle-de-sport.jpg" />
<meta property="twitter:image" content="https://cache.cosmopolitan.fr/data/photo/w1200_h630_ci/5b/femme-salle-de-sport.jpg" />
<meta property="og:description" content="Une étudiante allemande de 22 ans s’est faite renvoyer de sa salle de sport par un coach jugeant que sa tenue n'était pas adaptée. Son nombril risquait de gêner les hommes... Oui oui, vous avez bien lu." />
<meta property="twitter:description" content="Une étudiante allemande de 22 ans s’est faite renvoyer de sa salle de sport par un coach jugeant que sa tenue n'était pas adaptée. Son nombril risquait de gêner les hommes... Oui oui, vous avez bien lu." />
<meta property="og:type" content="article" />
<meta property="fb:pages" content="114291737956" />
<meta name="description" content="Une étudiante allemande de 22 ans s’est faite renvoyer de sa salle de sport par un coach jugeant que sa tenue n'était pas adaptée. Son nombril risquait de gêner les hommes... Oui oui, vous avez bien lu." />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="theme-color" content="#e6007e" />
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
<link rel="canonical" href="https://www.cosmopolitan.fr/une-jeune-femme-viree-de-sa-salle-de-sport-car-sa-tenue-perturbait-les-hommes,2026050.asp"/>
<link rel="shortcut icon" href="https://www.cosmopolitan.fr/favicon.ico"/>
<link rel="apple-touch-icon" href="https://cache.cosmopolitan.fr/image/pwa/icon-192x192.png">
<script>
if ('serviceWorker' in navigator) {
	window.addEventListener('load', function() {
		navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
			// Registration was successful
			console.log('ServiceWorker registration successful with scope: ', registration.scope);
		}).catch(function(err) {
			// registration failed :(
			console.log('ServiceWorker registration failed: ', err);
		});
	});
}
// Setup a listener to track Add to Homescreen events.
window.addEventListener('appinstalled', (evt) => {
	triggerAnalyticsEvent(
		"Clic",
		"Installation via mini-info bar",
		"pwa"
	);
});
</script>

</head>

<body itemscope itemtype="https://schema.org/WebPage" data-entry-id="2006" data-thematic-id="2511247" data-section-id="2511248">


<!--@@@layout@@@-->
<header id="header" data-area-name="header">
	
<!--@@@zone@@@-->

<!--@@@consentManagement@@@-->

<!--@@@consentManagement@@@-->


<!--@@@dataManagementPlateform@@@-->


<!--@@@dataManagementPlateform@@@-->


<!--@@@advertising@@@-->

<!--@@@advertising@@@-->


<!--@@@SiteHeader@@@-->
<div class="SiteHeader">
	<ul class="SiteHeader-links">
		<li class="SiteHeader-linksItem SiteHeader-linksItem--social">
			<div class="SiteHeader-link-socialContainer">
<!--@@@socialNetwork@@@-->
<div class="SocialLinks SocialLinks--header">
<div class="SocialLinks-linkContainer"><a href="https://www.facebook.com/Cosmopolitan.fr" class="SocialLinks-link SocialLinks-link--facebook" target="_blank" title=""></a></div><div class="SocialLinks-linkContainer"><a href="https://twitter.com/Cosmopolitan_fr" class="SocialLinks-link SocialLinks-link--twitter" target="_blank" title=""></a></div><div class="SocialLinks-linkContainer"><a href="https://www.pinterest.fr/Cosmopolitanfr/" class="SocialLinks-link SocialLinks-link--pinterest" target="_blank" title=""></a></div><div class="SocialLinks-linkContainer"><a href="https://www.instagram.com/cosmopolitan_fr/" class="SocialLinks-link SocialLinks-link--instagram" target="_blank" title=""></a></div><div class="SocialLinks-linkContainer"><a href="https://www.snapchat.com/add/cosmopolitan_fr" class="SocialLinks-link SocialLinks-link--snapchat" target="_blank" title=""></a></div><div class="SocialLinks-linkContainer"><a href="https://www.cosmopolitan.fr/newsletter" class="SocialLinks-link SocialLinks-link--newsletter"  title=""></a></div></div>

<!--@@@socialNetwork@@@-->
</div>
		</li>
	</ul>
	<div class="SiteHeader-authentication">
		<div class="SiteHeader-authenticationLinkContainer">
			<button class="SiteHeader-authenticationLink js-showSignUpModal">Inscription</button>
		</div>
		<div class="SiteHeader-authenticationLinkContainer">
			<button class="SiteHeader-authenticationLink js-showLogInModal">Connexion</button>
		</div>
	</div>
	<div class="SiteHeader-container">
		<div class="SiteHeader-contentContainer">
			<div class="SiteHeader-content">
				<button class="SiteHeader-menuButton js-toggleRetractableMenu">Menu</button>
									<a href="https://www.cosmopolitan.fr" id="logo" class="SiteHeader-logo">Cosmopolitan.fr : Beaut&eacute;, Mode, Sexo</a>
								<div class="SiteHeader-magazineSubscription">
<!--@@@magazine@@@-->
<div class="MagazineSubscription MagazineSubscription--header js-MagazineSubscription--header">
	<a href="https://www.cosmopolitan.fr/abonnement" class="MagazineSubscription-link">Je lis le magazine</a>
		<a href="https://www.cosmopolitan.fr/abonnement" data-burger-url="https://www.cosmopolitan.fr/abonnement" class="MagazineSubscription-popup MagazineSubscription-popup--subscriptionLink">
		<div class="MagazineSubscription-popup--content">
			<div class="MagazineSubscription-popup--imageContainer" data-burger-src="https://cache.cosmopolitan.fr/data/photo/w245_c17/magazine/cover.jpg?817783963130fa49ffff1e5777328534">
				<span class="MagazineSubscription-popup--image js-MagazineSubscription-popup--image" src="https://cache.cosmopolitan.fr/data/photo/w274_c17/magazine/cover.jpg?817783963130fa49ffff1e5777328534"></span>
				<div class="MagazineSubscription-popup--pastille"><p>Jusqu'à</p>-50%</div>
			</div>
			<div class="MagazineSubscription-popup--links">
											<div class="MagazineSubscription-popup--link version-papier"><p>version</p><p>papier</p></div>
													<div class="MagazineSubscription-popup--link version-numerique"><p>version</p><p>numérique</p></div>
									</div>
			<div class="MagazineSubscription-popup--button">J'ach&egrave;te au num&eacute;ro</div>
			<div class="MagazineSubscription-popup--button MagazineSubscription-popup--buttonWithoutTopMargin">Je m'abonne</div>
			<div class="MagazineSubscription-popup--subtitle"><p>Jusqu'à <bold>50%</bold> de réduction</p><p>et de nombreux cadeaux <p>au choix</p></p></div>
		</div>
	</a>
	</div>

<!--@@@magazine@@@-->
</div>
				<button class="SiteHeader-searchButton js-showSearchOverlay"></button>
			</div>
		</div>
	</div>
<div class="SiteHeader-search js-siteOverlay">
	<div class="SiteHeader-searchContent">
		
<!--@@@title@@@-->
<div class="Title Title--overlay SiteHeader-searchTitle"><span class="Title-content">Recherche sur Cosmopolitan.fr</span></div>
<!--@@@title@@@-->
		<button class="SiteHeader-searchCloseButton js-hideSearchOverlay"></button>
		<div class="SiteHeader-searchFormContainer">
			<form method="get" action="https://www.cosmopolitan.fr/recherche" class="SiteHeader-searchForm">
				<input type="text" name="q" class="SiteHeader-searchField SiteHeader-searchQueryField js-searchQueryField" placeholder="Tapez ici votre recherche" />
				<input type="submit" value="OK" class="SiteHeader-searchField SiteHeader-searchSubmitButton" />
			</form>
		</div>
	</div>
</div>
</div>

<!--@@@SiteHeader@@@-->


<!--@@@authentication@@@-->
<div class="Modal js-authenticationModal">
	<div class="AuthenticationModal Modal-container">
		<div class="Modal-header">
			<div class="Modal-title">Connectez-vous</div>
			<button class="Modal-close js-closeModal"></button>
		</div>
		<div class="Modal-body">
			<div class="AuthenticationModal-description js-authenticationModalDescription"></div>
			<div class="AuthenticationModal-formsContainer" >
				<form method="post" action="https://www.cosmopolitan.fr/direct/membre/identification" class="AuthenticationModal-logInContainer js-logInForm" novalidate="novalidate">
					<input type="hidden" name="redirectURL" value="referer" />
					<div class="AuthenticationModal-signInText">
						<div class="AuthenticationModal-signInTitle">Vous avez d&eacute;j&agrave; un compte</div>

					</div>
					<div class="AuthenticationModal-logInError js-logInError"></div>
					<div class="AuthenticationModal-fieldContainer AuthenticationModal-fieldContainer--logInIdentifier">
						<input type="email" autocomplete="username" name="memberLogin" placeholder="Adresse e-mail" class="AuthenticationModal-field AuthenticationModal-logInIdentifier js-logInEmailAddress" />
					</div><!--
					--><div class="AuthenticationModal-fieldContainer AuthenticationModal-fieldContainer--logInPassword">
						<input type="password" autocomplete="current-password" name="memberPassword" placeholder="Mot de passe" class="AuthenticationModal-field AuthenticationModal-logInPassword" />
						<div class="AuthenticationModal-lostPasswordContainer">
							<button type="button" class="AuthenticationModal-lostPassword js-sendLostPassword">Mot de passe oubli&eacute;</button>
						</div>
					</div>
					<div class="AuthenticationModal-fieldContainer AuthenticationModal-fieldContainer--logInKeepLoggedIn">
						<input type="checkbox" name="setCookie" id="AuthenticationModal-setCookie"><!--
						--><label for="AuthenticationModal-setCookie">Se souvenir de moi</label>
					</div>
					<div class="AuthenticationModal-fieldContainer AuthenticationModal-fieldContainer--logInSubmit">
						<input type="submit" value="OK" />
					</div>
				</form>

				<div class="AuthenticationModal-signUpContainer">

					<div class="AuthenticationModal-signUpText">
													<div class="AuthenticationModal-signUpTitle">Vous n'avez pas encore de compte</div>
														<div class="AuthenticationModal-signUpLead">Je cr&eacute;&eacute; un compte pour recevoir mes newsletters et g&eacute;rer mes donn&eacute;es personnelles.</div>
												</div>

											<form method="get" action="https://www.cosmopolitan.fr/inscription" class="AuthenticationModal-signUpForm AuthenticationModal-signUpForm--defaultSignUp is-visible js-signUpForm" novalidate="novalidate">

							<input type="hidden" name="redirectURL" value="referer" />

							<div class="AuthenticationModal-fieldContainer AuthenticationModal-fieldContainer--signUpEmailAddress">
								<input type="email" placeholder="Adresse e-mail" name="emailAddress" class="AuthenticationModal-field AuthenticationModal-signUpEmailAddress js-signUpEmailAddress" />
							</div>
							<div class="AuthenticationModal-fieldContainer AuthenticationModal-fieldContainer--signUpSubmit">
								<input type="submit" value="CRÉER MON COMPTE" />
							</div>

						</form>
						
					<form method="post" action="https://www.cosmopolitan.fr/direct/membre/inscriptionrapide" class="AuthenticationModal-signUpForm AuthenticationModal-signUpForm--quickSignUp  js-signUpForm" novalidate="novalidate">
						<input type="hidden" name="redirectURL" value="referer" class="js-AuthenticationRedirectURL" />
						<input type="text" value="" name="applyRegistration" class="applyRegistration" />

						<div class="AuthenticationModal-fieldContainer AuthenticationModal-fieldContainer--signUpEmailAddress">
							<div>
								<input type="email" placeholder="Adresse e-mail" name="emailAddress" class="AuthenticationModal-field AuthenticationModal-signUpEmailAddress js-signUpEmailAddress" />
							</div>
							<div>
								<input type="text" placeholder="Pseudonyme" name="nickname" class="AuthenticationModal-field AuthenticationModal-signUpNickName js-signUpNickName" />
							</div>
														<div class="AuthenticationModal-optionContainer">
								<label for="agreement6217e22667166">
									<input type="checkbox" class="js-signUpAgreement" id="agreement6217e22667166" /> J'accepte la <a href="" target="_blank">charte</a>
								</label>
							</div>
							<div class="AuthenticationModal-optionContainer">
								<label for="optin6217e22667167">
									<input type="checkbox" value="1" name="optin" id="optin6217e22667167" /> Je souhaite recevoir des offres du site et ses partenaires
								</label>
							</div>

						</div><!--
						--><div class="AuthenticationModal-fieldContainer AuthenticationModal-fieldContainer--signUpSubmit">
							<input type="submit" value="OK" class="AuthenticationModal-signUpSubmit" />
						</div>
					</form>



				</div>
			</div>

							<div class="AuthenticationModal-externalLogIns">
					<div class="AuthenticationModal-externalLogInLabel">
						Utiliser mon compte&nbsp;:
					</div>
					<div class="AuthenticationModal-externalLogInContainer">
						<button type="button" class="AuthenticationModal-externalLogIn AuthenticationModal-externalLogIn--facebook js-externalLogIn" data-login-service="facebook">Facebook</button>
					</div>
				</div>
			
		</div>

		

	</div>
</div>
<script>
mc2m.push(function(){
	loadAuthentication([
		"\/data\/display\/authentication.js?drem9wwiqj48cw4080w488ogw",
		"\/data\/display\/modal.css?6zpupk5t6pcsgg0k4wkscokkk"	]);
});
</script>

<!--@@@authentication@@@-->

<nav class="Menu">
	<div class="Menu-wrapper is-static clearfix">
		<ul class="Menu-list">
<li class="Menu-item Menu-item--section Menu-item--section-2004" data-weight="100"><a href="https://www.cosmopolitan.fr/beaute-forme,2004.asp2" class="Menu-link">Beaut&eacute;</a></li><li class="Menu-item Menu-item--section Menu-item--section-2002" data-weight="90"><a href="https://www.cosmopolitan.fr/mode,2002.asp2" class="Menu-link">Mode</a></li><li class="Menu-item Menu-item--section Menu-item--section-2008" data-weight="80"><a href="https://www.cosmopolitan.fr/love-sexe,2008.asp2" class="Menu-link">Sexo</a></li><li class="Menu-item Menu-item--section Menu-item--section-2006 is-active Menu-item--parentItem" data-weight="70"><a href="https://www.cosmopolitan.fr/moi-moi-moi,2006.asp2" class="Menu-link">Psycho</a>				<ul class="Menu-sectionList">
					<li class="Menu-sectionItem Menu-sectionItem--2012">
						<a href="https://www.cosmopolitan.fr/,toutes-les-actus-moi-moi-moi,2077.htm" class="Menu-sectionLink">Actu psycho</a>
					</li>
					<li class="Menu-sectionItem Menu-sectionItem--2016">
						<a href="https://www.cosmopolitan.fr/psycho,2016.asp1" class="Menu-sectionLink">D&eacute;veloppement personnel</a>
						<ul class="Menu-subSectionList">
							<li class="Menu-subSectionItem Menu-subSectionItem--2511069">
								<a href="https://www.cosmopolitan.fr/,se-connaitre,2511069.htm" class="Menu-subSectionLink">Se conna&icirc;tre</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511070">
								<a href="https://www.cosmopolitan.fr/,le-bonheur-c-est-maintenant,2511070.htm" class="Menu-subSectionLink">&Ecirc;tre heureuse</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511071">
								<a href="https://www.cosmopolitan.fr/,gerer-son-stress,1906912.asp" class="Menu-subSectionLink">G&eacute;rer son stress</a>
							</li>
						</ul>
					</li>
					<li class="Menu-sectionItem Menu-sectionItem--2511377">
						<a href="https://www.cosmopolitan.fr/bien-avec-les-autres,2511377.asp1" class="Menu-sectionLink">Bien avec les autres</a>
						<ul class="Menu-subSectionList">
							<li class="Menu-subSectionItem Menu-subSectionItem--2511072">
								<a href="https://www.cosmopolitan.fr/,bien-avec-les-autres,2511072.htm" class="Menu-subSectionLink">Relations sociales</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511378">
								<a href="https://www.cosmopolitan.fr/amitie,2511378.htm" class="Menu-subSectionLink">Amiti&eacute;</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511379">
								<a href="https://www.cosmopolitan.fr/relations-familiales,2511379.htm" class="Menu-subSectionLink">Relations familiales</a>
							</li>
						</ul>
					</li>
					<li class="Menu-sectionItem Menu-sectionItem--2019">
						<a href="https://www.cosmopolitan.fr/,tous-les-tests-et-quiz-moi-moi-moi,2086.htm" class="Menu-sectionLink">Tests &amp; quiz psycho</a>
					</li>
					<li class="Menu-sectionItem Menu-sectionItem--2511247">
						<a href="https://www.cosmopolitan.fr/societe,2511247.asp1" class="Menu-sectionLink">Soci&eacute;t&eacute;</a>
						<ul class="Menu-subSectionList">
							<li class="Menu-subSectionItem Menu-subSectionItem--2511248">
								<a href="https://www.cosmopolitan.fr/,societe,2511248.htm" class="Menu-subSectionLink">Actus soci&eacute;t&eacute;</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511408">
								<a href="https://www.cosmopolitan.fr/droits-des-femmes,2511408.htm" class="Menu-subSectionLink">Droits des femmes</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511409">
								<a href="https://www.cosmopolitan.fr/libertes,2511409.htm" class="Menu-subSectionLink">Libert&eacute;s</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511410">
								<a href="https://www.cosmopolitan.fr/planete,2511410.htm" class="Menu-subSectionLink">Plan&egrave;te</a>
							</li>
						</ul>
					</li>
					<li class="Menu-sectionItem Menu-sectionItem--2015">
						<a href="https://www.cosmopolitan.fr/,tous-les-articles-vos-confession,2510609.htm" class="Menu-sectionLink">Mes confessions</a>
					</li>
					<li class="Menu-sectionItem Menu-sectionItem--2018">
						<a href="https://www.cosmopolitan.fr/,toutes-les-cosmolistes-moi-moi-moi,2085.htm" class="Menu-sectionLink">Cosmoliste</a>
					</li>
					<li class="Menu-sectionItem Menu-sectionItem--2010">
						<a href="https://www.cosmopolitan.fr/job-argent,2010.asp1" class="Menu-sectionLink">Job</a>
						<ul class="Menu-subSectionList">
							<li class="Menu-subSectionItem Menu-subSectionItem--2049">
								<a href="https://www.cosmopolitan.fr/,job,2049.htm" class="Menu-subSectionLink">Trouver un job</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511399">
								<a href="https://www.cosmopolitan.fr/reussir-sa-carriere,2511399.htm" class="Menu-subSectionLink">R&eacute;ussir sa carri&egrave;re</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511401">
								<a href="https://www.cosmopolitan.fr/s-epanouir-au-travail,2511401.htm" class="Menu-subSectionLink">S'&eacute;panouir au travail</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511402">
								<a href="https://www.cosmopolitan.fr/relations-professionnelles,2511402.htm" class="Menu-subSectionLink">Relations professionnelles</a>
							</li>
							<li class="Menu-subSectionItem Menu-subSectionItem--2511400">
								<a href="https://www.cosmopolitan.fr/reussir-ses-etudes,2511400.htm" class="Menu-subSectionLink">R&eacute;ussir ses &eacute;tudes</a>
							</li>
						</ul>
					</li>
				</ul>
</li><li class="Menu-item Menu-item--section Menu-item--section-2511090" data-weight="60"><a href="https://www.cosmopolitan.fr/forme,2511090.asp2" class="Menu-link">Forme</a></li><li class="Menu-item Menu-item--section Menu-item--section-2009" data-weight="50"><a href="https://www.cosmopolitan.fr/astro/" class="Menu-link">Astro</a></li><li class="Menu-item Menu-item--section Menu-item--section-2511052" data-weight="40"><a href="https://www.cosmopolitan.fr/lifestyle,2511052.asp2" class="Menu-link">Lifestyle</a></li><li class="Menu-item Menu-item--section Menu-item--section-2005" data-weight="30"><a href="https://www.cosmopolitan.fr/people,2005.asp2" class="Menu-link">People</a></li><li class="Menu-item Menu-item--section Menu-item--section-2007" data-weight="20"><a href="https://www.cosmopolitan.fr/culture,2007.asp2" class="Menu-link">Culture</a></li><li class="Menu-item is-hidden" data-weight="10"><a href="https://www.cosmopolitan.fr/video" class="Menu-link">Vid&eacute;os</a></li><li class="Menu-item is-hidden" data-weight="8"><a href="https://www.cosmopolitan.fr/newsletter" class="Menu-link">Newsletters</a></li><li class="Menu-item is-hidden" data-weight="7"><a href="https://www.cosmopolitan.fr/,les-concours,2511305.htm" class="Menu-link">Jeux concours</a></li><li class="Menu-item Menu-item--section Menu-item--section-2511320 is-hidden" data-weight="6"><a href="https://www.cosmopolitan.fr/,boutique,2511320.htm" class="Menu-link">Boutique</a></li><li class="Menu-item is-hidden" data-weight="5"><a href="https://www.cosmopolitan.fr/code-promo/" class="Menu-link">Codes Promo</a></li>		</ul>
	</div>
</nav>
<script>
mc2m.push(function() { jQuery(document).trigger("menuAvailable"); });
</script>


<!--@@@zone@@@-->
</header>
<div id="body" data-area-name="page">
	<div class="BodyContent">
		
<!--@@@zone@@@-->

<!--@@@advertising@@@-->
<div class="External External--top-2 External-container is-empty" rel="top-2"><div id="top-2" class="External-container" data-format-id="5984"><script>//sas_manager.render(5984);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 5984, tagId: "top-2"});
</script>
</div>

<!--@@@advertising@@@-->



<!--@@@zone@@@-->

<!--@@@filariane@@@-->
<nav class="Breadcrumb clearfix">
	<ul class="Breadcrumb-list" itemscope itemtype="https://schema.org/BreadcrumbList">
		<li class="Breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a href="https://www.cosmopolitan.fr/" class="Breadcrumb-link" itemprop="item"><span itemprop="name">Cosmopolitan</span></a><meta itemprop="position" content="1" /></li>
		<li class="Breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a href="https://www.cosmopolitan.fr/moi-moi-moi,2006.asp2" class="Breadcrumb-link" itemprop="item"><span itemprop="name">Psycho</span></a><meta itemprop="position" content="2" /></li>
		<li class="Breadcrumb-item" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a href="https://www.cosmopolitan.fr/societe,2511247.asp1" class="Breadcrumb-link" itemprop="item"><span itemprop="name">Soci&eacute;t&eacute;</span></a><meta itemprop="position" content="3" /></li>
		<li class="Breadcrumb-item is-active" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem"><a href="https://www.cosmopolitan.fr/,societe,2511248.htm" class="Breadcrumb-link" itemprop="item"><span itemprop="name">Actus soci&eacute;t&eacute;</span></a><meta itemprop="position" content="4" /></li>
	</ul>
</nav>

<!--@@@filariane@@@-->


<!--@@@articleContainer@@@-->
<article data-area-name="content">
	<div data-area-name="header">

<!--@@@zone@@@-->


<!--@@@articleHeader@@@-->
<div class="ArticleHeader">

<!--@@@title@@@-->
<h1 class="Title Title--article ArticleHeader-title"><span class="Title-content">Une jeune femme vir&eacute;e de sa salle de sport car sa tenue&nbsp;&quot;perturbait&quot; les hommes</span></h1>
<!--@@@title@@@-->
	<div class="ArticleHeader-metaContainer">
		<div class="ArticleHeader-metaElement ArticleHeader-authorContainer">
			Par <span class="ArticleHeader-author"><a class="ArticleHeader-authorLink" href="https://www.cosmopolitan.fr/mylene-wascowiski-journaliste-presse-ecrite-et-video,2031171.asp">Mylène Wascowiski</a></span>
		</div>
	</div>
	<div class="ArticleHeader-imageContainer">
		<figure><div class="ArticleHeader-imageAndCopyright ">
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w1000_ci/5b/femme-salle-de-sport.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w2000_ci/5b/femme-salle-de-sport.webp 2000w, https://cache.cosmopolitan.fr/data/photo/w1536_ci/5b/femme-salle-de-sport.webp 1536w, https://cache.cosmopolitan.fr/data/photo/w768_ci/5b/femme-salle-de-sport.webp 768w, https://cache.cosmopolitan.fr/data/photo/w680_ci/5b/femme-salle-de-sport.webp 680w" type="image/webp" sizes="(min-width: 1020px) 1000px, (max-width: 1019px) and (min-width: 640px) 100vw, (max-width: 639px) 100vw"/><img  src="https://cache.cosmopolitan.fr/data/photo/w1000_ci/5b/femme-salle-de-sport.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w1000_ci/5b/femme-salle-de-sport.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w2000_ci/5b/femme-salle-de-sport.jpg 2000w, https://cache.cosmopolitan.fr/data/photo/w1536_ci/5b/femme-salle-de-sport.jpg 1536w, https://cache.cosmopolitan.fr/data/photo/w768_ci/5b/femme-salle-de-sport.jpg 768w, https://cache.cosmopolitan.fr/data/photo/w680_ci/5b/femme-salle-de-sport.jpg 680w" sizes="(min-width: 1020px) 1000px, (max-width: 1019px) and (min-width: 640px) 100vw, (max-width: 639px) 100vw" alt="une femme &agrave; la salle de sport" class="ArticleHeader-image" /></picture>
<!--@@@imageTag@@@-->
			<button class="ArticleHeader-bookmark js-bookmarkArticle" title="Ajouter aux favoris">Ajouter aux favoris</button>
			<button class="ArticleHeader-copyrightContainer"><span class="ArticleHeader-copyright">BraunS / Istock</span></button>

<!--@@@socialNetwork@@@-->
<div class="SocialNetwork SocialNetwork--light" data-title="Une jeune femme vir&eacute;e de sa salle de sport car sa tenue&nbsp;&quot;perturbait&quot; les hommes">
	<button class="SocialNetwork-service SocialNetwork-service--pinterest js-SocialNetworkLink" data-type="pinterest" data-image-url="https://cache.cosmopolitan.fr/data/photo/w2000_ci/5b/femme-salle-de-sport.webp"></button>
</div>
<!--@@@socialNetwork@@@-->
</div>
		</figure>

	</div>

<!--@@@socialNetwork@@@-->
<div class="SocialNetwork" data-title="Une jeune femme vir&eacute;e de sa salle de sport car sa tenue&nbsp;&quot;perturbait&quot; les hommes" data-area-name="social">

		<div class="SocialNetwork-serviceContainer">
	<button class="SocialNetwork-service SocialNetwork-service--facebook js-SocialNetworkLink" data-type="facebook"></button>
	<button class="SocialNetwork-service SocialNetwork-service--twitter js-SocialNetworkLink" data-type="twitter"></button>
	<button class="SocialNetwork-service SocialNetwork-service--pinterest js-SocialNetworkLink" data-type="pinterest" data-image-url="https://cache.cosmopolitan.fr/data/photo/w2000_ci/5b/femme-salle-de-sport.webp"></button>
	<button class="SocialNetwork-service SocialNetwork-service--sms js-SocialNetworkLink" data-type="sms"></button>
	<button class="SocialNetwork-service SocialNetwork-service--whatsapp js-SocialNetworkLink" data-type="whatsapp"></button>
	<button class="SocialNetwork-service SocialNetwork-service--email js-SocialNetworkLink" data-type="email"></button>
		</div>
</div>

<!--@@@socialNetwork@@@-->
<div class="Modal js-SendEmail">
	<div class="SendEmail Modal-container">
		<form method="post" action="https://www.cosmopolitan.fr/direct/community/sendmailtofriend" class="SendEmail-form js-SendEmailForm">
			<input type="hidden" name="url" value="https://www.cosmopolitan.fr/une-jeune-femme-viree-de-sa-salle-de-sport-car-sa-tenue-perturbait-les-hommes,2026050.asp" />
			<div class="Modal-header">
				<div class="Modal-title SendEmail-title">Envoyer par e-mail</div>
				<button class="Modal-close js-closeModal" type="button"></button>
			</div>
			<div class="Modal-body">
				<fieldset class="SendEmail-group SendEmail-group--sender">
					<div class="SendEmail-sectionLabel SendEmail-sectionLabel--sender">Vous</div>
					<div class="SendEmail-element">
						<label class="SendEmail-label" for="SendEmailFirstNameField">De</label>
						<input type="text" name="firstName" placeholder="Votre pr&eacute;nom" class="SendEmail-input SendEmail-firstNameField" id="SendEmailFirstNameField" />
					</div>
					<div class="SendEmail-element">
						<label class="SendEmail-label" for="SendEmailEmailAddressField">Adresse e-mail</label>
						<input type="text" name="from" placeholder="Votre adresse e-mail" class="SendEmail-input SendEmail-emailAddressField" id="SendEmailEmailAddressField" />
					</div>
				</fieldset>
				<fieldset class="SendEmail-group SendEmail-group--recipients js-SendEmailRecipientList">
					<div class="SendEmail-sectionLabel SendEmail-sectionLabel--recipients">Vos destinataires<button class="SendEmail-addRecipient js-SendEmailAddRecipient" type="button">+</button></div>
					<div class="SendEmail-element js-SendEmailRecipient">
						<label class="SendEmail-label">Adresse e-mail</label>
						<input type="text" name="to[]", placeholder="Adresse e-mail du destinataire" class="SendEmail-input SendEmail-recipientField" />
					</div>
				</fieldset>
				<div class="SendEmail-group SendEmail-group--captcha">
					<span><input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="void" style=" left:0px; top:0px;  width: 20px ;
		top:-5351px;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="972c028a5338e26df38b1c9e9218c96e" style=" left:0px; top:0px;  width: 20px ;
		display: none;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="b4d4fb376bd7a19076bb251fdadc0b67" style=" left:0px; top:0px;  width: 20px ;
		visibility:hidden;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="1" type="checkbox" name="emailToFriendcontrol[]" value="e6e63954dea37a178e17cd9e25c490a0" style=" left:0px; top:0px;  width: 20px ;
		visibility:hiddene; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="ef9c275724ce4a398d0414427301c0a3" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="65de67de9714aa87e55b2ae6f3e677e5" style=" left:0px; top:0px;  width: 20px ;
		top:-5728px;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="6de0641590de8014505ea850ca700662" style=" left:0px; top:0px;  width: 20px ;
		display: none;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="a2f0ae3dd48296483ca2e88082adb29a" style=" left:0px; top:0px;  width: 20px ;
		left:-5091px;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="282070e38408a4560e204a21ba74743f" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="630f5fa1adb65f4d1e986664951bfe10" style=" left:0px; top:0px;  width: 20px ;
		z-index:-5949;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="fc37d9ea51a409f6e8ce81744a7c2e25" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="052a44b1e91b3de64ed88cb0e32f7b7a" style=" left:0px; top:0px;  width: 20px ;
		visibility:hidden;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="35f524306e35bb9424bdc0742d091bed" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="362cfc5ec17c2387071d6450618e7385" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="b20bdc1f07da49bf2ca4377627cb8292" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="0dc89e32990f4d6580772f8102395509" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="7609baa9d8ffe9b678064554f38af66a" style=" left:0px; top:0px;  width: 20px ;
		z-index:-5653;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="4a13e45077621c11ba38b302fec6aa33" style=" left:0px; top:0px;  width: 20px ;
		z-index:-5189;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="e3fd8764589cdfcc65f1acfd9a44e1fc" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="b2850cbe40a86680d4d724d38b3ba20c" style=" left:0px; top:0px;  width: 20px ;
		visibility:hidden;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="eca1f169d1823ae83ee1cbbffd2342d0" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="57a56a373d00c9204e0b056187e00d01" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
</span><input autocomplete="off" type="hidden" id="emailToFriendpublic" name="emailToFriendpublic" value="13"/> <span class="SendEmail-captchaLabal">Je certifie ne pas envoyer d'e-mail ind&eacute;sirable</span>
				</div>
			</div>
			<div class="Modal-footer">
				<div class="Modal-buttons">
					<input type="submit" class="SendEmail-submitButton Modal-button" value="Envoyer" />
				</div>
			</div>
		</form>
	</div>
</div>
<!--@@@socialNetwork@@@-->

<!--@@@socialNetwork@@@-->
	<div class="ArticleHeader-lead">Une &eacute;tudiante allemande de 22 ans s&rsquo;est faite renvoyer de sa salle de sport par un coach jugeant que sa tenue n'&eacute;tait pas adapt&eacute;e. Son nombril risquait de g&ecirc;ner les hommes... Oui oui, vous avez bien lu.</div>
</div>

<!--@@@articleHeader@@@-->


<!--@@@zone@@@-->
	</div>
	<div class="ArticleContent">
		<div class="ArticleContent-aside ArticleContent-advertising ArticleContent-advertising--primary thinColumn">


<!--@@@zone@@@-->

<!--@@@advertising@@@-->
<div class="External External--right-1 External-container is-empty" rel="right-1"><div id="right-1" class="External-container" data-format-id="385"><script>//sas_manager.render(385);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 385, tagId: "right-1"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@zone@@@-->
		</div>
		<div class="ArticleContent-aside ArticleContent-advertising ArticleContent-advertising--secondary thinColumn">


<!--@@@zone@@@-->

<!--@@@advertising@@@-->
<div class="External External--square-2 External-container is-empty" rel="square-2"><div id="square-2" class="External-container" data-format-id="97926"><script>//sas_manager.render(97926);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 97926, tagId: "square-2"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@zone@@@-->
		</div>
		<div class="ArticleContent-main mediumColumn" data-area-name="body">

<!--@@@articleContentContainer@@@-->
<div class="Article">
<div class="Article-textContainer Article-text "><p><a href="https://www.cosmopolitan.fr/,boundaries-le-sexisme-ordinaire-denonce-dans-une-serie-de-photos,1916749.asp" title="Boundaries, le sexisme ordinaire d&eacute;nonc&eacute; dans une s&eacute;rie de photos" class="internal"><strong>Sexisme ordinaire</strong></a>, bonjour... Le 22 janvier 2019, Marnie, &eacute;tudiante allemande de 22 ans, tweetait : "je viens juste de me faire virer de ma salle de sport car mes <a href="https://www.cosmopolitan.fr/comment-porter-la-mode-sportswear,2022048.asp" title="Comment porter la mode sportswear&nbsp;?" class="internal">v&ecirc;tements de sport</a> n&rsquo;&eacute;taient pas assez couvrants (je vous laisse voir la photo) et perturbaient les hommes venus faire du sport. A quel si&egrave;cle vit-on d&eacute;j&agrave; ? Tellement triste &raquo;.</p>
<blockquote class="twitter-tweet" data-lang="fr"><a href="https://twitter.com/may_reads/status/1087704002073108481?ref_src=twsrc%5Etfw"></a></blockquote>

<h2>"Tu ne peux pas t&rsquo;entra&icirc;ner ainsi"</h2>
<p>Une <strong>coach</strong> du club a ainsi expliqu&eacute; &agrave; la jeune femme originaire de Constance, dans le sud de l'Allemagne, qu&rsquo;elle ne &ldquo;pouvait pas s&rsquo;entra&icirc;ner ainsi&rdquo;. L&rsquo;objet du d&eacute;lit : un legging noir remont&eacute; en haut du nombril et un <a href="https://www.cosmopolitan.fr/,cropped-top-comment-le-porter,1899692.asp" title="Comment porter le crop top&nbsp;?" class="internal">crop top</a> laissant apercevoir quelques centim&egrave;tres de peau.</p>
<p>Dans un entretien accord&eacute; &agrave; Metro UK, la jeune femme explique "ils m&rsquo;ont dit que les hommes n&rsquo;avaient pas le droit de porter des <strong>d&eacute;bardeurs</strong> non plus, mais &agrave; chaque fois que j&rsquo;ai eu l&rsquo;occasion d&rsquo;aller dans cette salle, ils portaient tous des d&eacute;bardeurs et aucun d'entre eux n&rsquo;a &eacute;t&eacute; pri&eacute; de partir".</p>
<!--@@@advertising@@@-->
<div class="External External--intext-1 External-container is-empty" rel="intext-1"><div id="intext-1" class="External-container" data-format-id="18740"><script>//sas_manager.render(18740);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 18740, tagId: "intext-1"});
</script>
</div>

<!--@@@advertising@@@-->

<h2>Les femmes, trop souvent discrimin&eacute;es en raison de leur tenue</h2>
<p>Suite au tweet de la jeune femme, les r&eacute;actions ont &eacute;t&eacute; nombreuses. Une jeune fille, qui a v&eacute;cu la m&ecirc;me situation dans son <strong>&eacute;cole</strong>, explique "j&rsquo;ai l&rsquo;impression de vivre &agrave; l&rsquo;&eacute;poque victorienne. Nous ne devrions pas les laisser nous qualifier de &laquo; distractions &raquo; pour les gar&ccedil;ons pour une tenue dans laquelle nous nous sentons &agrave; l&rsquo;aise".</p>
<blockquote class="twitter-tweet" data-lang="fr"><a href="https://twitter.com/mollieparki_/status/1088342560333090817?ref_src=twsrc%5Etfw"></a></blockquote>
<p>
<script src="https://platform.twitter.com/widgets.js" async="" charset="utf-8"></script></p>
<p>Ce n&rsquo;est malheureusement pas la premi&egrave;re fois qu&rsquo;une jeune femme se voit discrimin&eacute;e en raison de sa <strong>tenue vestimentaire</strong>. On se souvient de de Remy Altuna, &eacute;tudiante &agrave; l&rsquo;&eacute;cole Baumont en Californie, dont le directeur l&rsquo;avait r&eacute;primand&eacute;e car elle ne portait pas de <a href="https://www.cosmopolitan.fr/,16-choses-que-votre-medecin-veut-que-vous-sachiez-sur-votre-soutien-gorge,1962341.asp" title="14 choses que votre m&eacute;decin veut que vous sachiez sur votre soutien-gorge" class="internal">soutien-gorge</a> sous son d&eacute;bardeur noir.</p>
<p>En France, une &eacute;l&egrave;ve de premi&egrave;re au lyc&eacute;e Flora Tristan (Seine-et-Marne) s&rsquo;&eacute;tait vu <strong>refuser l&rsquo;acc&egrave;s &agrave; son &eacute;tablissement</strong> en raison d&rsquo;une robe jug&eacute;e &laquo; trop longue &raquo;, pouvant &ecirc;tre consid&eacute;r&eacute;e comme un &laquo; signe ostentatoire religieux &raquo;. Une aberration en 2019.</p>

<!--@@@contextuallinks@@@-->
	<div class="ContextualLinks">
					<div class="ContextualLinks-title">Lire aussi :</div>
						<ul class="ContextualLinks-items"><li class="ContextualLinks-item">
						<a class="ContextualLinks-link" href="https://www.cosmopolitan.fr/januhairy-le-challenge-qui-invite-les-femmes-a-ne-pas-s-epiler-en-janvier,2025705.asp">Januhairy&nbsp;: le challenge qui invite les femmes &agrave; ne pas s'&eacute;piler en janvier</a>
					</li><li class="ContextualLinks-item">
						<a class="ContextualLinks-link" href="https://www.cosmopolitan.fr/sport-pro-les-femmes-gagnent-vraiment-beaucoup-moins-que-les-hommes,2025924.asp">Sport pro&nbsp;: les femmes gagnent (vraiment) beaucoup moins que les hommes</a>
					</li>			</ul></div>
	
<!--@@@contextuallinks@@@--></div></div>
<script type="application/ld+json">
	{"@context":"http:\/\/schema.org","@type":"NewsArticle","name":"Une jeune femme vir\u00e9e de sa salle de sport car sa tenue\u00a0\"perturbait\" les hommes","headline":"Une jeune femme vir\u00e9e de sa salle de sport car sa tenue\u00a0\"perturbait\" les hommes","description":"Une \u00e9tudiante allemande de 22 ans s\u2019est faite renvoyer de sa salle de sport par un coach jugeant que sa tenue n'\u00e9tait pas adapt\u00e9e. Son nombril risquait de g\u00eaner les hommes... Oui oui, vous avez bien lu.","mainEntityOfPage":"https:\/\/www.cosmopolitan.fr\/une-jeune-femme-viree-de-sa-salle-de-sport-car-sa-tenue-perturbait-les-hommes,2026050.asp","datePublished":"2019-01-30 11:07:18.000","dateModified":"2019-01-30T11:22:07+01:00","image":"https:\/\/cache.cosmopolitan.fr\/data\/photo\/w1200_h630_c17\/5b\/femme-salle-de-sport.jpg","author":[{"@type":"Person","name":"Myl\u00e8ne Wascowiski","url":"https:\/\/www.cosmopolitan.fr\/mylene-wascowiski-journaliste-presse-ecrite-et-video,2031171.asp"}],"publisher":{"@type":"Organization","name":"Cosmopolitan.fr","url":"https:\/\/www.cosmopolitan.fr","logo":{"@type":"ImageObject","url":"https:\/\/cache.cosmopolitan.fr\/image\/picto\/standard\/logo_organization.png"}}}
</script>
<!--@@@articleContentContainer@@@-->
		</div>
		<div class="ArticleContent-main ArticleContent-share mediumColumn" data-area-name="footer">

<!--@@@zone@@@-->

<!--@@@socialNetwork@@@-->
<div class="SocialNetwork SocialNetwork--condensed" data-title="Une jeune femme vir&eacute;e de sa salle de sport car sa tenue&nbsp;&quot;perturbait&quot; les hommes" data-area-name="social">

		<div class="SocialNetwork-serviceContainer">
	<button class="SocialNetwork-service SocialNetwork-service--facebook js-SocialNetworkLink" data-type="facebook"></button>
	<button class="SocialNetwork-service SocialNetwork-service--twitter js-SocialNetworkLink" data-type="twitter"></button>
	<button class="SocialNetwork-service SocialNetwork-service--pinterest js-SocialNetworkLink" data-type="pinterest"></button>
	<button class="SocialNetwork-service SocialNetwork-service--sms js-SocialNetworkLink" data-type="sms"></button>
	<button class="SocialNetwork-service SocialNetwork-service--whatsapp js-SocialNetworkLink" data-type="whatsapp"></button>
	<button class="SocialNetwork-service SocialNetwork-service--email js-SocialNetworkLink" data-type="email"></button>
		</div>
</div>

<!--@@@socialNetwork@@@-->
<div class="Modal js-SendEmail">
	<div class="SendEmail Modal-container">
		<form method="post" action="https://www.cosmopolitan.fr/direct/community/sendmailtofriend" class="SendEmail-form js-SendEmailForm">
			<input type="hidden" name="url" value="https://www.cosmopolitan.fr/une-jeune-femme-viree-de-sa-salle-de-sport-car-sa-tenue-perturbait-les-hommes,2026050.asp" />
			<div class="Modal-header">
				<div class="Modal-title SendEmail-title">Envoyer par e-mail</div>
				<button class="Modal-close js-closeModal" type="button"></button>
			</div>
			<div class="Modal-body">
				<fieldset class="SendEmail-group SendEmail-group--sender">
					<div class="SendEmail-sectionLabel SendEmail-sectionLabel--sender">Vous</div>
					<div class="SendEmail-element">
						<label class="SendEmail-label" for="SendEmailFirstNameField">De</label>
						<input type="text" name="firstName" placeholder="Votre pr&eacute;nom" class="SendEmail-input SendEmail-firstNameField" id="SendEmailFirstNameField" />
					</div>
					<div class="SendEmail-element">
						<label class="SendEmail-label" for="SendEmailEmailAddressField">Adresse e-mail</label>
						<input type="text" name="from" placeholder="Votre adresse e-mail" class="SendEmail-input SendEmail-emailAddressField" id="SendEmailEmailAddressField" />
					</div>
				</fieldset>
				<fieldset class="SendEmail-group SendEmail-group--recipients js-SendEmailRecipientList">
					<div class="SendEmail-sectionLabel SendEmail-sectionLabel--recipients">Vos destinataires<button class="SendEmail-addRecipient js-SendEmailAddRecipient" type="button">+</button></div>
					<div class="SendEmail-element js-SendEmailRecipient">
						<label class="SendEmail-label">Adresse e-mail</label>
						<input type="text" name="to[]", placeholder="Adresse e-mail du destinataire" class="SendEmail-input SendEmail-recipientField" />
					</div>
				</fieldset>
				<div class="SendEmail-group SendEmail-group--captcha">
					<span><input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="void" style=" left:0px; top:0px;  width: 20px ;
		left:-5393px;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="b2850cbe40a86680d4d724d38b3ba20c" style=" left:0px; top:0px;  width: 20px ;
		visibility:hidden;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="b4d4fb376bd7a19076bb251fdadc0b67" style=" left:0px; top:0px;  width: 20px ;
		top:-5500px;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="e3fd8764589cdfcc65f1acfd9a44e1fc" style=" left:0px; top:0px;  width: 20px ;
		z-index:-5524;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="e6e63954dea37a178e17cd9e25c490a0" style=" left:0px; top:0px;  width: 20px ;
		top:-5540px;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="ef9c275724ce4a398d0414427301c0a3" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="0dc89e32990f4d6580772f8102395509" style=" left:0px; top:0px;  width: 20px ;
		visibility:hidden;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="b20bdc1f07da49bf2ca4377627cb8292" style=" left:0px; top:0px;  width: 20px ;
		left:-5377px;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="282070e38408a4560e204a21ba74743f" style=" left:0px; top:0px;  width: 20px ;
		left:-5279px;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="35f524306e35bb9424bdc0742d091bed" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="052a44b1e91b3de64ed88cb0e32f7b7a" style=" left:0px; top:0px;  width: 20px ;
		display: none;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="fc37d9ea51a409f6e8ce81744a7c2e25" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="630f5fa1adb65f4d1e986664951bfe10" style=" left:0px; top:0px;  width: 20px ;
		display: none;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="362cfc5ec17c2387071d6450618e7385" style=" left:0px; top:0px;  width: 20px ;
		top:-5600px;;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="a2f0ae3dd48296483ca2e88082adb29a" style=" left:0px; top:0px;  width: 20px ;
		visibility:hidden;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="6de0641590de8014505ea850ca700662" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="65de67de9714aa87e55b2ae6f3e677e5" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="7609baa9d8ffe9b678064554f38af66a" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="4a13e45077621c11ba38b302fec6aa33" style=" left:0px; top:0px;  width: 20px ;
		visibility:hidden;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="972c028a5338e26df38b1c9e9218c96e" style=" left:0px; top:0px;  width: 20px ;
		z-index:-3;position:absolute; display:none; width:15px!important;
		"/>
<input autocomplete="off" tabindex="1" type="checkbox" name="emailToFriendcontrol[]" value="eca1f169d1823ae83ee1cbbffd2342d0" style=" left:0px; top:0px;  width: 20px ;
		lefte:-5500px;; width:15px!important;
		"/>
<input autocomplete="off" tabindex="100" type="checkbox" name="emailToFriendcontrol[]" value="57a56a373d00c9204e0b056187e00d01" style=" left:0px; top:0px;  width: 20px ;
		visibility:hidden;position:absolute; display:none; width:15px!important;
		"/>
</span><input autocomplete="off" type="hidden" id="emailToFriendpublic" name="emailToFriendpublic" value="19"/> <span class="SendEmail-captchaLabal">Je certifie ne pas envoyer d'e-mail ind&eacute;sirable</span>
				</div>
			</div>
			<div class="Modal-footer">
				<div class="Modal-buttons">
					<input type="submit" class="SendEmail-submitButton Modal-button" value="Envoyer" />
				</div>
			</div>
		</form>
	</div>
</div>
<!--@@@socialNetwork@@@-->

<!--@@@socialNetwork@@@-->


<!--@@@zone@@@-->
		</div>
	</div>

<!--@@@zone@@@-->

<!--@@@advertising@@@-->
<div class="External External--video-in-player External-container is-empty" rel="video-in-player"><div id="video-in-player" class="External-container" data-format-id="70829"><script>//sas_manager.render(70829);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 70829, tagId: "video-in-player"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@advertising@@@-->
<div class="External External--video-in-image External-container is-empty" rel="video-in-image"><div id="video-in-image" class="External-container" data-format-id="71387"><script>//sas_manager.render(71387);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 71387, tagId: "video-in-image"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@zone@@@-->
</article>
<!--@@@articleContainer@@@-->


<!--@@@newsletter@@@-->
<div class="NewsletterBox NewsletterBox--section">
	<div class="NewsletterBox-text">
		<p class="NewsletterBox-title">Recevez notre newsletter</p>
		<p class="NewsletterBox-subTitle">Quatre rendez-vous hebdos pour quatre fois plus de Cosmo</p>
	</div>
	<div class="NewsletterBox-formContainer">
		<form action="https://www.cosmopolitan.fr/newsletter" class="NewsletterBox-form js-NewsletterSectionSubscriptionForm" id="Newsletter-form" novalidate="novalidate">
						<input type="email" name="emailAddress" placeholder="Votre adresse email" class="NewsletterBox-emailAddressField js-emailAddress" />
			<input type="submit" value="Valider" class="NewsletterBox-submitButton NewsletterSectionButtonGGN" />
			<input type="submit" value="S'abonner" class="NewsletterBox-submitButton isConnected NewsletterSectionButtonGGN" />
		</form>
	</div>
</div>
<!--@@@newsletter@@@-->

<div class="ContextualVideo">
	<div class="ContextualVideo-videoContainer">
<!--@@@MediaPlayer@@@-->
<div id="m6217404e2bde9330399153" class="MediaPlayer" data-event-monitoring-category="Bloc" data-event-monitoring-action="Dailymotion"><script type="text/javascript">
mc2m.push(function()
{
	jQuery("#m6217404e2bde9330399153").mediaPlayer({"type":"Dailymotion","id":"x6xxjen","title":"Quel sport choisir selon son signe astrologique\u00a0?","autoPlayOnScroll":true,"mute":true,"muteAutoPlay":true,"advertisingParameters":{"use":"article-contextual"},"parameters":{"specific":{"Dailymotion":{"params":{"syndicationKey":129556,"enableInfo":false},"playerConfigurationId":"xgi8"},"Digiteka":{"desktop":{"mdtk":"01081965","zone":null},"mobile":{"mdtk":"01082107","zone":null}},"VideoJS":{"advertisingVastUrl":"https:\/\/pubads.g.doubleclick.net\/gampad\/live\/ads?iu=\/21720049534\/cosmopolitan_video_atf&description_url=https%3A%2F%2Fwww.cosmopolitan.fr%2F&env=vp&impl=s&correlator=&tfcd=0&npa=0&gdfp_req=1&output=vast&sz=640x480&unviewed_position_start=1"}}},"width":"100%","height":"100%"});
});
</script>
</div>

<!--@@@MediaPlayer@@@-->
</div>
	<div class="ContextualVideo-title">Quel sport choisir selon son signe astrologique&nbsp;?</div>
</div>


<!--@@@advertising@@@-->
<div class="External External--contextual-1 External-container is-empty" rel="contextual-1"></div>

<!--@@@advertising@@@-->


<!--@@@articleList@@@-->

<!--@@@list@@@-->
<div class="List List--articles List--inline">

<!--@@@title@@@-->
<div class="Title Title--contextual List-title"><span class="Title-content">
<!--@@@zone@@@-->
&Agrave; lire dans Psycho

<!--@@@zone@@@-->
</span></div>
<!--@@@title@@@-->
	<div class="List-items">
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/algorithme-trouver-prenom-bebe.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/algorithme-trouver-prenom-bebe.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/algorithme-trouver-prenom-bebe.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/algorithme-trouver-prenom-bebe.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/algorithme-trouver-prenom-bebe.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/algorithme-trouver-prenom-bebe.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/algorithme-trouver-prenom-bebe.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/algorithme-trouver-prenom-bebe.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/algorithme-trouver-prenom-bebe.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="un algorithme pour trouver le pr&eacute;nom de son b&eacute;b&eacute;" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/algorithme-pour-trouver-le-prenom-de-son-enfant,2055446.asp" class="ArticleLink-link">Cet algorithme vous donne le pr&eacute;nom de votre futur enfant</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/footballeuses-ame-ricaines-e-galite-salariale1.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/footballeuses-ame-ricaines-e-galite-salariale1.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/footballeuses-ame-ricaines-e-galite-salariale1.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/footballeuses-ame-ricaines-e-galite-salariale1.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/footballeuses-ame-ricaines-e-galite-salariale1.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/footballeuses-ame-ricaines-e-galite-salariale1.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/footballeuses-ame-ricaines-e-galite-salariale1.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/footballeuses-ame-ricaines-e-galite-salariale1.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/footballeuses-ame-ricaines-e-galite-salariale1.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="Egalit&eacute; salariale footballeuses am&eacute;ricaines" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/les-footballeuses-americaines-bientot-autant-payees-que-les-hommes,2055443.asp" class="ArticleLink-link">Les footballeuses am&eacute;ricaines bient&ocirc;t autant pay&eacute;es que les hommes</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/test-de-grossesse.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/test-de-grossesse.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/test-de-grossesse.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/test-de-grossesse.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/test-de-grossesse.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/test-de-grossesse.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/test-de-grossesse.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/test-de-grossesse.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/test-de-grossesse.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="Test de grossesse" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/avortement-le-delai-de-l-ivg-allonge-a-14-semaines,2055434.asp" class="ArticleLink-link">IVG&nbsp;: le d&eacute;lai officiellement allong&eacute; &agrave; 14 semaines&nbsp;!</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
	</div>
</div>

<!--@@@list@@@-->

<!--@@@articleList@@@-->


<!--@@@zone@@@-->


<!--@@@articleList@@@-->

<!--@@@list@@@-->
<div class="List List--articles">

<!--@@@title@@@-->
<div class="Title Title--contextual List-title"><span class="Title-content">Plus de <a href="https://www.cosmopolitan.fr/societe,2511247.asp1">Société</a></span></div>
<!--@@@title@@@-->
	<div class="List-items">
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--stacked ArticleLink--typed ArticleLink--gallery js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/podcasts-a-e-couter-pour-s-e-duquer.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w2000_h1000_ci/66/podcasts-a-e-couter-pour-s-e-duquer.webp 2000w, https://cache.cosmopolitan.fr/data/photo/w1536_h768_ci/66/podcasts-a-e-couter-pour-s-e-duquer.webp 1536w, https://cache.cosmopolitan.fr/data/photo/w768_h384_ci/66/podcasts-a-e-couter-pour-s-e-duquer.webp 768w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/podcasts-a-e-couter-pour-s-e-duquer.webp 680w" type="image/webp" sizes="(min-width: 1020px) 1000px, (max-width: 1019px) and (min-width: 640px) 100vw, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/podcasts-a-e-couter-pour-s-e-duquer.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/podcasts-a-e-couter-pour-s-e-duquer.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w2000_h1000_ci/66/podcasts-a-e-couter-pour-s-e-duquer.jpg 2000w, https://cache.cosmopolitan.fr/data/photo/w1536_h768_ci/66/podcasts-a-e-couter-pour-s-e-duquer.jpg 1536w, https://cache.cosmopolitan.fr/data/photo/w768_h384_ci/66/podcasts-a-e-couter-pour-s-e-duquer.jpg 768w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/podcasts-a-e-couter-pour-s-e-duquer.jpg 680w" sizes="(min-width: 1020px) 1000px, (max-width: 1019px) and (min-width: 640px) 100vw, (max-width: 639px) 100vw" alt="Podcasts &agrave; &eacute;couter pour s'&eacute;duquer " class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/les-podcasts-feministes-a-ecouter-pour-s-eduquer,2055425.asp" class="ArticleLink-link">6 podcasts f&eacute;ministes &agrave; &eacute;couter pour s&rsquo;&eacute;duquer</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
	</div>
</div>

<!--@@@list@@@-->

<!--@@@articleList@@@-->


<!--@@@articleList@@@-->

<!--@@@list@@@-->
<div class="List List--articles List--inline">
	<div class="List-items">
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/prenom-garcon-serie-netflix.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/prenom-garcon-serie-netflix.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/prenom-garcon-serie-netflix.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/prenom-garcon-serie-netflix.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/prenom-garcon-serie-netflix.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/prenom-garcon-serie-netflix.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/prenom-garcon-serie-netflix.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/prenom-garcon-serie-netflix.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/prenom-garcon-serie-netflix.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="prenom gar&ccedil;on s&eacute;rie netflix" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/les-prenoms-de-garcon-inspires-des-series-netflix,2055391.asp" class="ArticleLink-link">10 pr&eacute;noms de gar&ccedil;on inspir&eacute;s de nos s&eacute;ries Netflix pr&eacute;f&eacute;r&eacute;es</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/chariot-sur-fond-jaune.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/chariot-sur-fond-jaune.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/chariot-sur-fond-jaune.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/chariot-sur-fond-jaune.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/chariot-sur-fond-jaune.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/chariot-sur-fond-jaune.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/chariot-sur-fond-jaune.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/chariot-sur-fond-jaune.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/chariot-sur-fond-jaune.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="chariots sur fond jaune" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/voici-la-raison-pour-laquelle-vos-produits-preferes-sont-en-rupture-de-stock,2055387.asp" class="ArticleLink-link">Voici la raison pour laquelle vos produits pr&eacute;f&eacute;r&eacute;s sont en rupture de stock</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/train-pour-l-egalite.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/train-pour-l-egalite.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/train-pour-l-egalite.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/train-pour-l-egalite.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/train-pour-l-egalite.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/66/train-pour-l-egalite.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/66/train-pour-l-egalite.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/66/train-pour-l-egalite.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/66/train-pour-l-egalite.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="train pour l &eacute;galit&eacute; " class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/le-train-pour-l-egalite-l-oreal-paris-stand-up,2055378.asp" class="ArticleLink-link">Le Train pour l'&Eacute;galit&eacute; passe par 8 gares de France du 26 f&eacute;vrier au 7 mars</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
	</div>
</div>

<!--@@@list@@@-->

<!--@@@articleList@@@-->


<!--@@@zone@@@-->


<!--@@@advertising@@@-->
<div class="External External--context-eshop-contenu External-container is-empty" rel="context-eshop-contenu"><div id="context-eshop-contenu" class="External-container" data-format-id="67579"><script>//sas_manager.render(67579);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 67579, tagId: "context-eshop-contenu"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@zone@@@-->


<!--@@@articleList@@@-->

<!--@@@list@@@-->
<div class="List List--articles">

<!--@@@title@@@-->
<div class="Title Title--contextual List-title"><span class="Title-content">Plus d'<a href="https://www.cosmopolitan.fr/,societe,2511248.htm">Actus société</a></span></div>
<!--@@@title@@@-->
	<div class="List-items">
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--stacked js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/celine-dion-lola-dubini-harcelement-scolaire.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w768_h384_ci/5b/celine-dion-lola-dubini-harcelement-scolaire.webp 768w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/celine-dion-lola-dubini-harcelement-scolaire.webp 680w, https://cache.cosmopolitan.fr/data/photo/w1197_h599_ci/5b/celine-dion-lola-dubini-harcelement-scolaire.webp 1197w" type="image/webp" sizes="(min-width: 1020px) 1000px, (max-width: 1019px) and (min-width: 640px) 100vw, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/celine-dion-lola-dubini-harcelement-scolaire.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/celine-dion-lola-dubini-harcelement-scolaire.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w768_h384_ci/5b/celine-dion-lola-dubini-harcelement-scolaire.jpg 768w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/celine-dion-lola-dubini-harcelement-scolaire.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w1197_h599_ci/5b/celine-dion-lola-dubini-harcelement-scolaire.jpg 1197w" sizes="(min-width: 1020px) 1000px, (max-width: 1019px) and (min-width: 640px) 100vw, (max-width: 639px) 100vw" alt="Celine Dion se confie &agrave; Lola Dubini sur le harc&egrave;lement scolaire" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/celine-dion-se-confie-a-lola-dubini-sur-son-experience-du-harcelement-scolaire,2026034.asp" class="ArticleLink-link">C&eacute;line Dion se confie &agrave; Lola Dubini sur son exp&eacute;rience du harc&egrave;lement scolaire</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
	</div>
</div>

<!--@@@list@@@-->

<!--@@@articleList@@@-->


<!--@@@articleList@@@-->

<!--@@@list@@@-->
<div class="List List--articles List--inline">
	<div class="List-items">
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/eggheadchallenge.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/eggheadchallenge.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/eggheadchallenge.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/5b/eggheadchallenge.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/eggheadchallenge.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/eggheadchallenge.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/eggheadchallenge.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/eggheadchallenge.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/5b/eggheadchallenge.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="EggHeadChallenge" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/eggheadchallenge-des-femmes-atteintes-de-cancer-devoilent-leur-crane-chauve-pour-la-bonne-cause,2025950.asp" class="ArticleLink-link">#EggHeadChallenge&nbsp;: des femmes atteintes de cancer d&eacute;voilent leur cr&acirc;ne chauve pour la bonne cause</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/jenesuispasjolie.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/jenesuispasjolie.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/jenesuispasjolie.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/5b/jenesuispasjolie.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/jenesuispasjolie.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/jenesuispasjolie.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/jenesuispasjolie.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/jenesuispasjolie.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/5b/jenesuispasjolie.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="Jenesuispasjolie L&eacute;a" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/la-youtubeuse-jenesuispasjolie-se-confie-sur-les-violences-gynecologiques-subies-pendant-son-avortement,2025861.asp" class="ArticleLink-link">La YouTubeuse Jenesuispasjolie se confie sur les violences gyn&eacute;cologiques subies pendant son avortement</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/lora-dicarlo-sextoy.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/lora-dicarlo-sextoy.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/lora-dicarlo-sextoy.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/5b/lora-dicarlo-sextoy.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/lora-dicarlo-sextoy.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/lora-dicarlo-sextoy.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/lora-dicarlo-sextoy.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/lora-dicarlo-sextoy.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/5b/lora-dicarlo-sextoy.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="Sexty os&eacute;" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/sexisme-dans-la-tech-un-sextoy-juge-immoral-se-voit-retirer-son-prix-a-las-vegas,2025772.asp" class="ArticleLink-link">Sexisme dans la tech&nbsp;: un sextoy jug&eacute; &quot;immoral&quot; se voit retirer son prix &agrave; Las Vegas</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/fauteuil-roulant.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/fauteuil-roulant.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/fauteuil-roulant.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/5b/fauteuil-roulant.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/fauteuil-roulant.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/fauteuil-roulant.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/fauteuil-roulant.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/fauteuil-roulant.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/5b/fauteuil-roulant.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="violence paraplegique" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/defenestree-par-son-compagnon-et-handicapee-on-la-considere-responsable,2025706.asp" class="ArticleLink-link">D&eacute;fenestr&eacute;e par son compagnon et handicap&eacute;e, on la consid&egrave;re &quot;responsable&quot;</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/januhairy.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/januhairy.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/januhairy.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/5b/januhairy.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/januhairy.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/5b/januhairy.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/5b/januhairy.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/5b/januhairy.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/5b/januhairy.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="poils acceptation de soi" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/januhairy-le-challenge-qui-invite-les-femmes-a-ne-pas-s-epiler-en-janvier,2025705.asp" class="ArticleLink-link">Januhairy&nbsp;: le challenge qui invite les femmes &agrave; ne pas s'&eacute;piler en janvier</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
		<div class="List-item">
<!--@@@articleLink@@@-->
<div class="ArticleLink ArticleLink--standard ArticleLink--small js-linkContainer ">
	<div class="ArticleLink-imageContainer">
		
<!--@@@imageTag@@@-->
<picture><source srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/1ec/pll-bienveillance.webp 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/1ec/pll-bienveillance.webp 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/1ec/pll-bienveillance.webp 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/1ec/pll-bienveillance.webp 320w" type="image/webp" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw"/><span  src="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/1ec/pll-bienveillance.jpg" srcset="https://cache.cosmopolitan.fr/data/photo/w500_h250_ci/1ec/pll-bienveillance.jpg 500w, https://cache.cosmopolitan.fr/data/photo/w1000_h500_ci/1ec/pll-bienveillance.jpg 1000w, https://cache.cosmopolitan.fr/data/photo/w680_h340_ci/1ec/pll-bienveillance.jpg 680w, https://cache.cosmopolitan.fr/data/photo/w320_h160_ci/1ec/pll-bienveillance.jpg 320w" sizes="(min-width: 1020px) 500px, (max-width: 1019px) and (min-width: 640px) 320px, (max-width: 639px) 100vw" alt="PLL bienveillance" class="ArticleLink-image js-delayedImageLoading"></span></picture>
<!--@@@imageTag@@@-->
	</div>
	<div class="ArticleLink-content">
		<div class="ArticleLink-contentHeader">
			<div class="ArticleLink-title"><a href="https://www.cosmopolitan.fr/et-le-mot-de-l-annee-est,2025488.asp" class="ArticleLink-link">Et le mot de l'ann&eacute;e est...</a></div>
		</div>
	</div>
</div>
<!--@@@articleLink@@@-->
</div>
	</div>
</div>

<!--@@@list@@@-->

<!--@@@articleList@@@-->


<!--@@@zone@@@-->


<!--@@@displayComments@@@-->

<!--@@@displayComments@@@-->


<!--@@@advertising@@@-->
<div class="External External--rectangle-3 External-container is-empty" rel="rectangle-3"><div id="rectangle-3" class="External-container" data-format-id="1511"><script>//sas_manager.render(1511);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 1511, tagId: "rectangle-3"});
</script>
</div>

<!--@@@advertising@@@-->

<div class="PreviousNextArticleContainer">
	<div class="PreviousNextArticleContent">
		<div class="PreviousNextArticle PreviousNextArticle-previous">
			<a href="https://www.cosmopolitan.fr/garder-sa-culotte-pendant-un-examen-gynecologue,2026049.asp" class="PreviousNextArticle-link PreviousNextArticle-link--previous">Garder sa culotte pendant un examen gyn&eacute;cologique</a>
		</div>
		<div class="PreviousNextArticle PreviousNextArticle-next">
			<a href="https://www.cosmopolitan.fr/celibataire-ou-en-couple-celine-dion-s-exprime-enfin,2026051.asp" class="PreviousNextArticle-link PreviousNextArticle-link--next">C&eacute;libataire ou en couple&nbsp;? C&eacute;line Dion s'exprime enfin</a>
		</div>
</div>
</div> 

<!--@@@populars@@@-->
<div class="Populars">

<!--@@@title@@@-->
<div class="Title Title--topContent Populars-title"><span class="Title-content">Les plus populaires</span></div>
<!--@@@title@@@-->
<a href="https://www.cosmopolitan.fr/prenoms-tendance-2050,2053916.asp" class="Populars-link" title="La liste des pr&eacute;noms qui seront tendance en 2050">La liste des pr&eacute;noms qui seront tendance en 2050</a><a href="https://www.cosmopolitan.fr/les-prenoms-refuses-par-l-etat-civil-en-2021,2055345.asp" class="Populars-link" title="Voici les pr&eacute;noms qui ont &eacute;t&eacute; refus&eacute;s par l&rsquo;&eacute;tat civil">Voici les pr&eacute;noms qui ont &eacute;t&eacute; refus&eacute;s par l&rsquo;&eacute;tat civil</a><a href="https://www.cosmopolitan.fr/prenoms-interdits-en-france,2054302.asp" class="Populars-link" title="Ces pr&eacute;noms qui sont interdits en France">Ces pr&eacute;noms qui sont interdits en France</a><a href="https://www.cosmopolitan.fr/prenom-garcon-rare,2055181.asp" class="Populars-link" title="60 pr&eacute;noms de gar&ccedil;on rares">60 pr&eacute;noms de gar&ccedil;on rares</a><a href="https://www.cosmopolitan.fr/liste-des-prenoms-qui-disparaissent,2055193.asp" class="Populars-link" title="Voici la liste des pr&eacute;noms qui pourraient bient&ocirc;t dispara&icirc;tre">Voici la liste des pr&eacute;noms qui pourraient bient&ocirc;t dispara&icirc;tre</a><a href="https://www.cosmopolitan.fr/prenom-mixte-pour-un-enfant,2054133.asp" class="Populars-link" title="35 jolis pr&eacute;noms mixtes &agrave; donner &agrave; un b&eacute;b&eacute;">35 jolis pr&eacute;noms mixtes &agrave; donner &agrave; un b&eacute;b&eacute;</a></div>

<!--@@@populars@@@-->


<!--@@@advertising@@@-->
<div class="External External--rectangle-2 External-container is-empty" rel="rectangle-2"><div id="rectangle-2" class="External-container" data-format-id="25687"><script>//sas_manager.render(25687);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 25687, tagId: "rectangle-2"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@zone@@@-->


<!--@@@zone@@@-->
	</div>
</div>
<footer id="footer" data-area-name="footer">
	
<!--@@@zone@@@-->

<!--@@@advertising@@@-->
<div class="External External--footer-1 External-container is-empty" rel="footer-1"><div id="footer-1" class="External-container" data-format-id="4923"><script>//sas_manager.render(4923);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 4923, tagId: "footer-1"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@miscellaneous@@@-->

<div class="FooterLogo">

</div>

<!--@@@miscellaneous@@@-->


<!--@@@socialNetwork@@@-->
<div class="SocialLinks SocialLinks--footer">
<div class="SocialLinks-linkContainer"><a href="https://www.facebook.com/Cosmopolitan.fr" class="SocialLinks-link SocialLinks-link--facebook" target="_blank" title=""></a></div><div class="SocialLinks-linkContainer"><a href="https://twitter.com/Cosmopolitan_fr" class="SocialLinks-link SocialLinks-link--twitter" target="_blank" title=""></a></div><div class="SocialLinks-linkContainer"><a href="https://www.pinterest.fr/Cosmopolitanfr/" class="SocialLinks-link SocialLinks-link--pinterest" target="_blank" title=""></a></div><div class="SocialLinks-linkContainer"><a href="https://www.instagram.com/cosmopolitan_fr/" class="SocialLinks-link SocialLinks-link--instagram" target="_blank" title=""></a></div><div class="SocialLinks-linkContainer"><a href="https://www.snapchat.com/add/cosmopolitan_fr" class="SocialLinks-link SocialLinks-link--snapchat" target="_blank" title=""></a></div><div class="SocialLinks-linkContainer"><a href="https://www.cosmopolitan.fr/newsletter" class="SocialLinks-link SocialLinks-link--newsletter"  title=""></a></div></div>

<!--@@@socialNetwork@@@-->


<!--@@@magazine@@@-->
<div class="MagazineSubscription MagazineSubscription-popin js-magazineSubscriptionPopin">
	<button class="MagazineSubscription-popin--Close js-hideMagazineSubscriptionPopin"></button>
	<a class="MagazineSubscription-popin--link" href="https://www.cosmopolitan.fr/abonnement" target="_blank">
				<span class="MagazineSubscription-popin--image js-MagazineSubscription-popin--image" src="https://cache.cosmopolitan.fr/data/photo/w175_c17/magazine/cover.jpg?817783963130fa49ffff1e5777328534"></span>
		<p class="MagazineSubscription-popin--text MagazineSubscription-popin--firstText">J'achète au numéro</p>		<p class="MagazineSubscription-popin--text">Je m'abonne</p>
		
	</a>
</div>

<!--@@@magazine@@@-->


<!--@@@newsletter@@@-->
<div class="NewsletterBox NewsletterBox--footer">
	<div class="NewsletterBox-title">Inscrivez-vous &agrave; la newsletter</div>
	<div class="NewsletterBox-formContainer">
		<form method="get" action="https://www.cosmopolitan.fr/newsletter" class="NewsletterBox-form" novalidate="novalidate">
			<input type="email" name="emailAddress" placeholder="Votre adresse e-mail" class="NewsletterBox-emailAddressField js-emailAddress" />
			<input type="submit" value="Valider" class="NewsletterBox-submitButton" />
		</form>
	</div>
</div>
<!--@@@newsletter@@@-->


<!--@@@miscellaneous@@@-->
<div class="FooterLinks">
	<ul class="FooterLinks-list">
<li class="FooterLinks-item"><a href="https://www.cosmopolitan.fr/plan/2006" class="FooterLinks-link" >Plan du site</a></li><li class="FooterLinks-item"><a href="https://www.cosmopolitan.fr/,conditions-generales-d-utilisation,2239,1002123.asp" class="FooterLinks-link" >Mentions l&eacute;gales</a></li><li class="FooterLinks-item js-openConsentManagementModal"><a href="https://www.cosmopolitan.fr/,politique-d-utilisation-des-cookies,1890383.asp" class="FooterLinks-link" >Cookies</a></li>	</ul>
</div>

<!--@@@miscellaneous@@@-->


<!--@@@zone@@@-->

<!--@@@advertising@@@-->
<div class="External External--habillage External-container is-empty" rel="habillage"><div id="habillage" class="External-container" data-format-id="6013"><script>//sas_manager.render(6013);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 6013, tagId: "habillage"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@advertising@@@-->
<div class="External External--dhtml External-container is-empty" rel="dhtml"><div id="dhtml" class="External-container" data-format-id="386"><script>//sas_manager.render(386);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 386, tagId: "dhtml"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@advertising@@@-->
<div class="External External--native-1 External-container is-empty" rel="native-1"><div id="native-1" class="External-container" data-format-id="40383"><script>//sas_manager.render(40383);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 40383, tagId: "native-1"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@advertising@@@-->
<div class="External External--native-2 External-container is-empty" rel="native-2"><div id="native-2" class="External-container" data-format-id="40384"><script>//sas_manager.render(40384);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 40384, tagId: "native-2"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@advertising@@@-->
<div class="External External--native-3 External-container is-empty" rel="native-3"><div id="native-3" class="External-container" data-format-id="76596"><script>//sas_manager.render(76596);</script></div>
<script>
var sas = sas || {}; sas.formats = sas.formats || [];
sas.formats.push({id: 76596, tagId: "native-3"});
</script>
</div>

<!--@@@advertising@@@-->


<!--@@@advertising@@@-->
 <div class="delayedAdvertising" rel="contextual-1" style="display: none"><aside>
	<div class="OUTBRAIN" data-src="https://www.cosmopolitan.fr/une-jeune-femme-viree-de-sa-salle-de-sport-car-sa-tenue-perturbait-les-hommes,2026050.asp" data-widget-id="AR_1"></div>
</aside>
</div>
<script type="text/javascript" class="replaceDelayedAds">
mc2m.push(function() { replaceDelayedAds(); });
</script>
<script type="text/javascript">
var sas = sas || {}; sas.cmd = sas.cmd || [];
var SmartAdserverString = '';
sas.siteId = 1654;
sas.pageId = 166286;
sas.headerBiddingUrl = "\/\/tagmanager.smartadserver.com\/50\/1654\/smart.prebid.js";
mc2m.push(function()
{
	sas.target = "entree=a2006b;thematique=a2511247b;rubrique=a2511248b;niveau=a5b;level=5;tds=a1b;idggn=a2026050b;connected=a" + (((typeof(pageEnvironment) === "object") && pageEnvironment.context.isConnected) ? "1" : "0") + "b;subscriber=a" + (((typeof(pageEnvironment) === "object") && pageEnvironment.context.isSubscriber) ? "1" : "0") + "b;route=" + ((typeof(pageEnvironment) === "object") ? pageEnvironment.route : "") + ";declination=" + ((typeof(getPageDeclinationName) === "function") ? getPageDeclinationName() : "") + ";subdomain=" + document.location.host.replace(/\.cosmopolitan\.fr/, "") + "" + ';' + SmartAdserverString;
});
</script>
<script>
mc2m.push(function()
{
	runOnUserConsent(function()
	{
		if (pageEnvironment.context.isSubscriber === false)
		{
			mc2m.loadResources({url: "https:\/\/widgets.outbrain.com\/outbrain.js?i=9b7baacd", attributes: {async: true}});
		}
	});
});
</script>

<!--@@@advertising@@@-->


<!--@@@zone@@@-->



<!--@@@zone@@@-->

<!--@@@adrenalead@@@-->

<!--@@@adrenalead@@@-->


<!--@@@zone@@@-->


<!--@@@zone@@@-->
</footer>

<!--@@@layout@@@-->
<script src="https://cache.cosmopolitan.fr/data/display/tools.js?1pt9mbm44a3o00cksgogw4w48"></script>
<script src="https://cache.cosmopolitan.fr/data/display/page.js?daf71vpporkgw0kwosww0g844"></script>
<script src="https://cache.cosmopolitan.fr/data/display/article.js?f3wehyr44o8osogo48ksc0k4g"></script>
<script>
!function(e){Object.defineProperty(e,"push",{value:function(e){"function"==typeof e&&e()}});for(var t;t=e.shift();)try{e.push(t)}catch(o){console.error(o)}}(window.mc2m)
</script>

<!--@@@googleTagManager@@@-->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TRTV46" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({"gtm.start":
new Date().getTime(),event:"gtm.js"});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!="dataLayer"?"&l="+l:"";j.async=true;j.src=
"//www.googletagmanager.com/gtm.js?id="+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,"script","dataLayer","GTM-TRTV46");</script>

<!--@@@googleTagManager@@@-->
<div style="height:0; overflow:hidden">
</div>
<!-- web-application-1 -->
</body>

</html>
