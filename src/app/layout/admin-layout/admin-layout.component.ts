import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthadminService } from 'src/app/views/services/authadmin.service';

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {
  nom:any
  tokenStorageService: any;
  constructor(private asd:AuthadminService,private route:Router) {
    this.nom=localStorage.getItem('nom')

   }

  ngOnInit(): void {
  }

  // logout(): void {
  //   this.tokenStorageService.signOut();
  //   window.location.reload();
  //   this.route.navigate(['/admin/loginadmin']);
  // }

}
