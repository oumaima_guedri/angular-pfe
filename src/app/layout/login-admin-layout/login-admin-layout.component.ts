import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthadminService } from 'src/app/views/services/authadmin.service';

@Component({
  selector: 'app-login-admin-layout',
  templateUrl: './login-admin-layout.component.html',
  styleUrls: ['./login-admin-layout.component.scss']
})
export class LoginAdminLayoutComponent implements OnInit {

  dataReceived:any
  url:any
  messageAuthError:any
  constructor(private asd:AuthadminService,private route:Router,private arouter:ActivatedRoute) { 
    if(this.asd.LoggedIn()==true){
      this.route.navigate(['/admin'])
  }
  }

  ngOnInit(): void {
    this.url=this.arouter.snapshot.queryParams['returnUrl'] || '/admin/'
    console.log(this.url)
  }
  loginadmin(f:any){
    let data=f.value

    this.asd.login(data).subscribe((response)=>
      {
        this.dataReceived=response
        this.asd.saveDataProfil(this.dataReceived.token)
        this.route.navigate([this.url])
        
      },err=>this.messageAuthError="invalid email and password")

  }

  //   let data=f.value
  //   this.asd.login(data).subscribe((response)=>
      
      
  //     {
  //         this.dataReceived=response
  //         console.log(this.dataReceived)
  //       this.asd.saveDataProfil(this.dataReceived.token,this.dataReceived.roles)
  //       console.log(this.asd.ProfilAdmin.email)
  //       console.log(this.asd.ProfilAdmin.nom)

      
  //         //console.log(response)
  //         console.log(this.asd.IsLoggedIn)
  //         this.route.navigate(['/admin/dashboard'])
  //     },
    
    
  //   err=>console.log(err))

  // }

}
