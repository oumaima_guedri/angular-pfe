import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { FrontLayoutComponent } from './front-layout/front-layout.component';
import { LoginAdminLayoutComponent } from './login-admin-layout/login-admin-layout.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AdminLayoutComponent,
    FrontLayoutComponent,
    LoginAdminLayoutComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule

  ]
})
export class LayoutModule { }
