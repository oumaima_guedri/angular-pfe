import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllutilisateurComponent } from './allutilisateur.component';

const routes: Routes = [
  {path:'',component:AllutilisateurComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllutilisateurRoutingModule { }
