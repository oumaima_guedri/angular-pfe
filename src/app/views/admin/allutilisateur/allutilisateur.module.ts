import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllutilisateurRoutingModule } from './allutilisateur-routing.module';
import { AllutilisateurComponent } from './allutilisateur.component';


@NgModule({
  declarations: [
    AllutilisateurComponent
  ],
  imports: [
    CommonModule,
    AllutilisateurRoutingModule,
   
  ]
})
export class AllutilisateurModule { }
