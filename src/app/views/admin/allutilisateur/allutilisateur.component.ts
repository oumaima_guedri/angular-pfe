import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-allutilisateur',
  templateUrl: './allutilisateur.component.html',
  styleUrls: ['./allutilisateur.component.scss']
})
export class AllutilisateurComponent implements OnInit {
dataArray:any
  constructor(private sd:DataService) { 
    this.sd.getAllutilisateur().subscribe(data=>this.dataArray=data)
  }

  ngOnInit(): void {
  }

}
