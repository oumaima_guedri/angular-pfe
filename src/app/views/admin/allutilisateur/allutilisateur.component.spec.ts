import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllutilisateurComponent } from './allutilisateur.component';

describe('AllutilisateurComponent', () => {
  let component: AllutilisateurComponent;
  let fixture: ComponentFixture<AllutilisateurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllutilisateurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllutilisateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
