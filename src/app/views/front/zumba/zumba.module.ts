import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZumbaRoutingModule } from './zumba-routing.module';
import { ZumbaComponent } from './zumba.component';


@NgModule({
  declarations: [
    ZumbaComponent
  ],
  imports: [
    CommonModule,
    ZumbaRoutingModule
  ]
})
export class ZumbaModule { }
