import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { Role } from 'src/app/modelinscr/role';
import { SignUpInfo } from 'src/app/modelinscr/signup-info';
import { AuthService } from 'src/app/serviceinsc/auth.service';

@Component({
  selector: 'app-registeruser',
  templateUrl: './registeruser.component.html',
  styleUrls: ['./registeruser.component.scss']
})
export class RegisteruserComponent implements OnInit {
  form: any = {};
  SignUpInfo: SignUpInfo;
  isSignUpFailed = false;
  errorMessage = '';

role:Role[];

  // role :Role[];
  // newId:any;
  // newRole :Role;



  constructor(private authService: AuthService , private router: Router) { }

  ngOnInit(): void {
    this.role =[
      {id:1 , name: "entraineur"},
      {id:2 , name: "abonne"},

    ]
    this.role=this.authService.listeRole();
  }
  onSubmit(){
    this.SignUpInfo = new SignUpInfo(this.form.nom, this.form.prenom,this.form.datenaiss, this.form.email,
                      this.form.password, this.form.telephone,this.form.poids, this.form.Role);
                      this.authService.signUp(this.SignUpInfo).pipe(first()).subscribe(
                        data  => {
                          this.isSignUpFailed = false ;
                          this.router.navigate (['loginuser'])
                        } ,
                        error => {
                          this.errorMessage = error.error.message ;
                          this.isSignUpFailed = true ;
                        }
                      )
   }

}
