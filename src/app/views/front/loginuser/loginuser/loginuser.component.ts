import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthuserService } from 'src/app/views/services/authuser.service';

@Component({
  selector: 'app-loginuser',
  templateUrl: './loginuser.component.html',
  styleUrls: ['./loginuser.component.scss']
})
export class LoginuserComponent implements OnInit {

  dataReceived:any
  constructor(private aus:AuthuserService,private route:Router) { }

  ngOnInit(): void {
  }

  login(f:any){
    let data=f.value
    this.aus.login(data).subscribe((response)=>
      
      
      {
          this.dataReceived=response
          console.log(this.dataReceived)
        this.aus.saveDataProfil(this.dataReceived.token,this.dataReceived.email,this.dataReceived.roles,this.dataReceived.nom)
        console.log(this.aus.ProfilUser.email)
        console.log(this.aus.ProfilUser.nom)

      
          //console.log(response)
          console.log(this.aus.IsLoggedIn)
          this.route.navigate(['/'])
      },
    
    
    err=>console.log(err))

  }

}
