import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";


@Injectable({
  providedIn: 'root'
})
export class AuthadminService {

  // ProfilAdmin={
  //   email:"",
  //   roles:"",
  //   nom:""
    
  // }
//IsLoggedIn:boolean=false
helper = new JwtHelperService();
role=''


  constructor(private http:HttpClient) { }

login(data:any) {
  return this.http.post('http://localhost:8080/api/auth/login',data)
}

saveDataProfil(token:any){
    
  //  let decodeToken= this.helper.decodeToken(token)
    
   localStorage.setItem('token',token)

  }
  getUsername(){
   let token:any=localStorage.getItem('token')
   let decodeToken= this.helper.decodeToken(token)

    return decodeToken.username

  }


  LoggedIn(){
     let token:any=localStorage.getItem('token')
     if(!token){
      return false
     }
     let decodeToken=this.helper.decodeToken(token)
    
    
     if(decodeToken.role!=='Admin'){
       return false
     }

     if(this.helper.isTokenExpired(token)){
       return false
     }

     return true
  }
  // localStorage.setItem('token',token)
  // localStorage.setItem('email',email)
  // localStorage.setItem('roles',roles)
  // localStorage.setItem('nom',nom)

  // this.ProfilAdmin.email=email
  // this.ProfilAdmin.nom=nom

  // this.IsLoggedIn=true

}



