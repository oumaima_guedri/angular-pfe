import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  

  constructor(private http:HttpClient) { }

  getAllutilisateur(){
    return this.http.get('http://localhost:8080/utilisateur/all')
  }
}
